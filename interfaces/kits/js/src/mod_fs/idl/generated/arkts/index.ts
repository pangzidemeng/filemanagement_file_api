
import {GlobalScope_ohos_file_fs} from "./OHGlobalScopeOhosFileFsMaterialized"
import {FILEIONativeModule} from "./FILEIONativeModule"

export class fs {
    public static rmdirSync(path: string): void {
        console.log("FILEIONativeModule start")
        new FILEIONativeModule()
        console.log("FILEIONativeModule end")
        GlobalScope_ohos_file_fs.rmdirSync(path)
        console.log("rmdirSync end")
        return
    }
}