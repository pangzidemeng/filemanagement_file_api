import { float64, int32, float32 } from "./types";
export declare function asFloat64(value: string): float64;
export declare function asString(value: float64 | undefined): string | undefined;
export declare function float32FromBits(value: int32): float32;
export declare function int32BitsFromFloat(value: float32): int32;
//# sourceMappingURL=double.d.ts.map