export declare function className(object?: Object): string;
export declare function isFunction(object?: Object): boolean;
export declare function functionOverValue<Value>(value: Value | (() => Value)): boolean;
export declare function refEqual<Value>(a: Value, b: Value): boolean;
export declare function isNotPrimitive(value: Object): boolean;
//# sourceMappingURL=ts-reflection.d.ts.map