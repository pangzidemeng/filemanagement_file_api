import { float64, int32, int8 } from "./types";
export declare function asArray<T>(value: T[]): Array<T>;
export declare function Array_from_set<T>(set: Set<T>): Array<T>;
export declare function Array_from_int32(data: Int32Array): int32[];
export declare function Array_from_number(data: float64[]): Array<float64>;
export declare function int8Array(size: int32): int8[];
//# sourceMappingURL=array.d.ts.map