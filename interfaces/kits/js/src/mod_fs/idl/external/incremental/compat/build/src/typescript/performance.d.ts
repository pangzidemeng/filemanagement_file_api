/**
 * @returns the number of milliseconds elapsed since midnight,
 *          January 1, 1970 Universal Coordinated Time (UTC).
 */
export declare function timeNow(): number;
/**
 * @param fractionDigits - number of digits after the decimal point [0 - 20]
 * @returns a string representing a number in fixed-point notation
 */
export declare function numberToFixed(value: number, fractionDigits: number): string;
//# sourceMappingURL=performance.d.ts.map